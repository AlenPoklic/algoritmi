package com.example.algoritmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_barvanje_grafov.*

class ActivityBarvanjeGrafov : AppCompatActivity() {

    var retval: String = ""
    val V: Int = 4
    var graph = arrayOf(
            booleanArrayOf(false, true, true, true),
            booleanArrayOf(true, false, true, false),
            booleanArrayOf(true, true, false, true),
            booleanArrayOf(true, false, true, false)
    )


    fun printMatrix(): String {
        var retString: String = ""
        var i: Int = 0
        while(i < 4) {
            var j: Int = 0
            while(j < 4) {
                if(graph[i][j])
                    retString+="1  "
                else
                    retString+="0  "
                j++
            }
            retString+="\n"
            i++
        }
        return retString
    }

    fun isSafe(v: Int, color: Array<Int>, c: Int): Boolean  {
        for(i in 0 until V) {
            if(graph[v][i] && c == color[i])
                return false
        }
        return true
    }

    fun printSolution(color: Array<Int>){
        retval += "Solution Exists: Following are the assigned colors \n"
        for (i in 0 until V)
            retval +="${color[i]} "
        retval +="\n"
    }


    fun graphColoringUntil(m: Int, color: Array<Int>, v: Int): Boolean  {
        if(v == V)
            return true

        for(c in 1..m+1) {
            if(isSafe(v, color, c)) {
                color[v] = c

                if (graphColoringUntil(m, color, v + 1))
                    return true

                color[v] = 0
            }
        }
        return false
    }

    fun graphColoring(m: Int): Boolean {
        var color = Array(V){0}
       // for(i in 0 until V)
           // color[i] = 0

        if(!graphColoringUntil(m,color,0)) {
            retval="Solution does not exist"
            return false
        }
        printSolution(color)
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barvanje_grafov)

        var grafi = arrayOf("Graf 1", "Graf 2")
        var barve = arrayOf(1,2,3,4)

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, grafi)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner1!!.adapter = aa

        val bb = ArrayAdapter(this, android.R.layout.simple_spinner_item, barve)
        bb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner3!!.adapter = bb

        spinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                return
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(spinner1.selectedItemId.toInt() == 0) {
                     graph = arrayOf(
                        booleanArrayOf(false, true, true, true),
                        booleanArrayOf(true, false, true, false),
                        booleanArrayOf(true, true, false, true),
                        booleanArrayOf(true, false, true, false)
                    )
                }
                else if(spinner1.selectedItemId.toInt() == 1) {
                     graph = arrayOf(
                        booleanArrayOf(false, true, false, false),
                        booleanArrayOf(true, false, false, true),
                        booleanArrayOf(false, false, false, false),
                        booleanArrayOf(false, true, false, false)
                    )
                }
                editText.setText(printMatrix())
            }
        }

        button4.setOnClickListener {
            retval = ""
            graphColoring  (spinner3.selectedItemPosition.toInt());
            val builder = AlertDialog.Builder(this@ActivityBarvanjeGrafov)
            builder.setTitle("Rešitve algoritma")
            builder.setMessage(retval)
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }

}
