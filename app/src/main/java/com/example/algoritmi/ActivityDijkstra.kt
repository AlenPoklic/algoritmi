package com.example.algoritmi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import java.io.File
import java.io.FileReader
import java.io.InputStream
import java.nio.file.Paths

import java.util.*
import android.widget.ArrayAdapter
import android.R.id
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_dijkstra.*


class ActivityDijkstra : AppCompatActivity() {

    var C = Array(30) { Array(30) {0} }
    var vel: Int = 0

    fun readFromFile(filename: String) {
        val fileText: String = applicationContext.assets.open(filename).bufferedReader().use {
            it.readText()
        }
        var first: Boolean = true
        for(line in fileText.lines()) {
            if(first) {
                vel = line.toInt();
            }
            else {
                // So  zamenjane vrstice pa stolpci ??????????????????
                C[line.split(" ")[1].toInt()-1][ line.split(" ")[0].toInt()-1] =  line.split(" ")[2].toInt()
            }
            first = false
        }
    }

    fun printMatrix(): String {
        var retval: String = ""
        var i: Int = 0
        while(i < vel) {
            var j: Int = 0
            while(j < vel) {
                if(C[i][j]<10)
                    retval+=" "
                retval+="${C[i][j]}  "
                if(C[i][j]<10)
                    retval+=" "
                j++
            }
            retval+="\n"
            i++
        }
        return retval
    }

    //Q: I was wondering if there is any way to define multiple variables in Kotlin at once?
    //A: This is not possible in Kotlin language.
    //src: https://stackoverflow.com/questions/40385878/define-multiple-variables-at-once-in-kotlin-e-g-java-string-x-y-z

    fun printVector(vec: Vector<Vozlisce>) {
        for(voz in vec) {
            voz.print()
        }
    }

    // https://stackoverflow.com/questions/44515031/is-kotlin-pass-by-value-or-pass-by-reference
    //  Q: Value or Reference?
    //  A: Both, ackchyually o-o
    fun reorder(vec: Vector<Vozlisce>, x: Int) {
        var temp: Vozlisce = vec[x];
        vec[x] = vec[x+1]
        vec[x+1] = temp
    }

    fun bubbleSort(vec: Vector<Vozlisce>) {
        var i: Int = 0;
        var j: Int = 0;
        var swapped: Boolean;

        // src: https://kotlinlang.org/docs/reference/control-flow.html

        while(i < vec.size) {
            swapped = false
            j = 0
            while(j < vec.size - i - 1) {
                if(vec[j].getDolzina() > vec[j+1].getDolzina()) {
                    reorder(vec, j)
                    swapped = true
                }
                j++
            }
            if(!swapped)
                break
            i++
        }
    }

    fun dijkstrov_algoritem(vec: Array<Vozlisce>, s: Int) {
        // Začetne nastavitve lastnosti
        for(i in 0..vel-1) {
            vec[i].setIme(i)
            vec[i].setDolzina(99999.9)
            vec[i].setPredhodnik(-1)
        }
        vec[s].setDolzina(0.0)

        // Vstavi vsa vozlisca v vrsto
        var q: Vector<Vozlisce> = Vector()
        for(i in 0..vel-1)
        //q.addElement(vec[i])
            q.add(vec[i])


        while(!q.isEmpty()) {
            bubbleSort(q)

//            for(i in 0 until vel-1)
//                println("Ime: ${vec[i].getIme()} Predhodnik: ${vec[i].getPredhodnik()} Dolzina: ${vec[i].getDolzina()}")
//            println("VRSTA")
//            for(i in 0 until q.size)
//                print("${q[i].getDolzina()} ")
//            println("")

            for(i in 0 until vel-1) {
                if(C[q[0].getIme()][i] != 0) {
                    if(vec[i].getDolzina() > q[0].getDolzina() + C[q[0].getIme()][i]) {
                        vec[i].setDolzina(q[0].getDolzina() + C[q[0].getIme()][i])
                        for(j in 0 until q.size) {
                            if(q[j].getIme() == vec[i].getIme())
                                q[j].setDolzina(vec[i].getDolzina())
                        }
                        vec[i].setPredhodnik(q[0].getIme())
                    }
                }
            }
            q.remove(q[0])
        }
    }

    var retval: String = ""

    fun izpisPoti(vec: Array<Vozlisce>, s: Vozlisce, v: Vozlisce) {
        if(s.getIme() == v.getIme())
            retval += "${s.getIme()+1}: dolzina: ${vec[s.getIme()].getDolzina()}\n"
        else if(vec[v.getIme()].getPredhodnik() == -1)
            retval += "Med vozliscem ${s.getIme()+1} in vozliscem ${v.getIme()+1} ni poti... \n"
        else {
            izpisPoti(vec,s,vec[v.getPredhodnik()])
            retval += "${v.getIme()+1}: dolzina ${vec[v.getIme()].getDolzina()}\n"
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dijkstra)

        /*


        // O vektorjih v kotlinu nisem našel nobene doumentacije
        val a = Vozlisce(1, 2, 3.0);
        val b = Vozlisce(1, 2, 7.0);
        val c = Vozlisce(1, 2, 1.0);
        val y: Vector<Vozlisce> = Vector()
        y.addElement(a)
        y.addElement(b)
        y.addElement(c)

        bubbleSort(y)

        printVector(y)

        // https://www.youtube.com/watch?v=lWd6TYhnJAk - KOTLIN FOR BEGINNERS - ARRAYS
        // https://www.ict.social/kotlin/basics/multidimensional-arrays-in-kotlin
        // https://stackoverflow.com/questions/45199704/kotlin-2d-array-initialization
        // "Drugačen" način deklaracije tabele
        var d = Array(30) { Array(30) {0} }
        println("\nArray : ${d[0][0]}\n")
        for (i: Int in 0 until d.size) {
            for (j: Int in 0 until d[i].size) {
                println("${d[i][j]} ")
            }
            println("\n")
        }
 */

        // KAKO BRATI IZ DATOTEKE?
        // https://www.youtube.com/watch?v=o5pDghyRHmI

        //ReadFromFile()

        // dijkstrov_algoritem(vec,0)


        var grafi = arrayOf("Graf 1", "Graf 2", "Graf 3")
       // var izhodisca = arrayOf()
      //  var cilji = arrayOf()
       // https://stackoverflow.com/questions/5219945/spinner-on-value-change/17871225

        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, grafi)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner1!!.adapter = aa




        spinner1.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                return
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if(spinner1.selectedItemId.toInt() == 0) {
                    readFromFile("graf1.txt")
                }
                else if(spinner1.selectedItemId.toInt() == 1) {
                    readFromFile("graf2.txt")
                }
                else if(spinner1.selectedItemId.toInt() == 2) {
                    readFromFile("graf3.txt")
                }

                val izhodisca = Array(vel) { i -> (i + 1).toString() }
                val cilji = Array(vel) { i -> (i + 1).toString() }

                val bb = ArrayAdapter(this@ActivityDijkstra, android.R.layout.simple_spinner_item, izhodisca)
                bb.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                val cc = ArrayAdapter(this@ActivityDijkstra, android.R.layout.simple_spinner_item, cilji)
                cc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

                editText.setText(printMatrix())
                spinner2!!.adapter = bb
                spinner3!!.adapter = cc
            }
        }


        button4.setOnClickListener {

            val vec = Array(vel){Vozlisce(0,0,0.0)}
            retval = ""

            dijkstrov_algoritem(vec,spinner2.selectedItemPosition.toInt())
            izpisPoti(vec,vec[spinner2.selectedItemPosition.toInt()],vec[spinner3.selectedItemPosition.toInt()])

            // https://android--code.blogspot.com/2018/02/android-kotlin-alertdialog-example.html

            val builder = AlertDialog.Builder(this@ActivityDijkstra)
            builder.setTitle("Najdene poti")
            builder.setMessage(retval)
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
    }

}
