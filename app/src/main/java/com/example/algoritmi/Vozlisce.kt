package com.example.algoritmi

// The primary constructor is part of the class header.
//The block of code surrounded by parentheses is the primary constructor
class Vozlisce(Ime: Int,Predhodnik: Int, Dolzina: Double) {
    private var ime: Int = Ime
    private var predhodnik: Int = Predhodnik
    private var dolzina: Double = Dolzina

    override fun toString():String {
        return "Ime:$ime Predhodnik:$predhodnik Dolzina:$dolzina"
    }

    // Če podaš kirega tipa je funkcija (:tip) more bi return
    fun print() {
        println("Ime:$ime Predhodnik:$predhodnik Dolzina:$dolzina")
    }

    fun getIme(): Int {
        return this.ime
    }

    fun getPredhodnik(): Int {
        return this.predhodnik
    }

    fun getDolzina(): Double {
        return this.dolzina
    }

    fun setIme(ime: Int) {
        this.ime = ime
    }

    fun setPredhodnik(predhodnik: Int) {
        this.predhodnik = predhodnik
    }

    fun setDolzina(dolzina: Double) {
        this.dolzina = dolzina
    }
}